# Nemotube (no longer works)

A stupidly-simple shell script which lets you search youtube, gets the URL to the video you select, and passes it to the player of your choice.

Made mostly for my own enjoyment (we hates slow, bloated, privacy-invading websites *Prrrecious*!), and also for educational purposes.

FYI: this hasn't worked in a long time. Leaving it up for posterity, probably won't ever work on it again.

Also, thanks so much Gitlab for deleting repos that haven't been touched in a year. Ugh.
